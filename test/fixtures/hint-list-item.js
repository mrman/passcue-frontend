// Fixture for a HN icon (hint url is what would trigger the HN icon to be shown)
exports.HN_HINT = {
  propsData: {
    hint: {url: 'news.ycombinator.com', text: "This is a ycombinator hint", created: new Date() }
  }
};

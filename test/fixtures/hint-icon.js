// Fixture for an unkonwn site icon (url would trigger, would come from the hint object in practice)
exports.UNKNOWN_SITE_HINT = {
  propsData: {
    url: 'example.com'
  }
};

// Fixture for a HN icon (hint url is what would trigger the HN icon to be shown)
exports.HN_HINT = {
  propsData: {
    url: 'news.ycombinator.com'
  }
};


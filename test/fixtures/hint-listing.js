// Fixture for a hint listing containing some hints to act on
exports.STATIC_HINTS = {
  propsData: {
    staticHints: [
      {url: 'news.ycombinator.com', text: "This is a ycombinator hint", created: new Date() },
      {url: 'google.com', text: "This is a google hint", created: new Date() }
    ]
  }
};

exports.STATIC_HINTS_WITH_FILTER = {
  propsData: {
    filterInput: "google",
    staticHints: [
      {url: 'news.ycombinator.com', text: "This is a ycombinator hint", created: new Date() },
      {url: 'google.com', text: "This is a google hint", created: new Date() }
    ]
  }
};

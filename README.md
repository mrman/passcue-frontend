# Passcue Frontend

The frontend for all web-technology based Passcue clients.

Looking for the backend that powers this project? Check out [passcue-api-web](https://gitlab.com/mrman/passcue-api-web).

## Prerequisites

You will need to download and install the following:

 - [nodejs](https://nodejs.org)
 - [npm](https://npmjs.com)
 - [jspm](https://jspm.io) `npm install jspm --save-dev`
 - [mocha](https://mochajs.org/) (for testing)

Also, if you want to run the full kit and kaboodle, you'll of course need to set up and run [passcue-api-web](https://gitlab.com/mrman/passcue-api-web).

## Installing

1. `npm install`
2. `jspm install`
3. `make`

## Running

To simply build all relevant UIs (web, chrome extension, etc)
    `make` (will run the first, `all` target in the makefile)



For local development, you can bundle and run the webapp (using node's `http-server`) by running:
    `make web-dev-watch`

To simply build the files required for each specific UI, use the following commands
    `make web`
    `make chrome-extension`

## Building

General instructions about the build process. Parts in build system:

  - [GNU Make](https://www.gnu.org/software/make/)
  - [entr](http://entrproject.org/) - Embedded in Make targets for watching/repackaging
  - jspm (see prerequisities section)

### Building the Chrome/Firefox Extension

To build the chrome/firefox extension:

`make web-extension`

### Building the Electron App

TODO

## Testing

Use mocha (run through make) to run tests


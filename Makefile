.PHONY: build-js html-web build-js-dev web-dev web-extension-zip web-extension \
				web serve-web web-watch test unit-test integration-test e2e-test \
				package package-docker \
				docker-container docker-instance docker-instance-remote clean-docker-instance

all: web web-extension package

build: web web-extension

MAKEFILE_DIR:=$(strip $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST)))))

JSPM = ./node_modules/.bin/jspm
HTTP_SERVER = ./node_modules/.bin/http-server
MAIN_MODULE = app/main

LOCAL_API_PORT = 4000
LOCAL_WEB_PORT = 4001

OUTPUT_FOLDER_WEB = target/web
OUTPUT_FOLDER_WEB_EXTENSION = target/web-extension
WEB_EXTENSION_ZIP_NAME = passcue-web-extension

JS_BUILD_OUTPUT_FILE = $(OUTPUT_FOLDER_WEB)/bundle.js
INDEX_PREBUILD = index.html
INDEX_PREBUILD_SFX = index-sfx.html
INDEX_POSTBUILD = $(OUTPUT_FOLDER_WEB)/index.html

METADATA = metadata
STATIC = static
WEB_EXTENSION_MANIFEST = $(METADATA)/manifest.json
EXTENSION_ICON = $(STATIC)/images/logo/passcue-logo-48x48.png

##########################
# Web Build (In-Browser) #
##########################

build-js:
	$(JSPM)	bundle-sfx $(MAIN_MODULE) $(JS_BUILD_OUTPUT_FILE) --minify

build-js-dev:
	$(JSPM)	bundle-sfx $(MAIN_MODULE) $(JS_BUILD_OUTPUT_FILE)

html-web:
	cp $(INDEX_PREBUILD_SFX) $(INDEX_POSTBUILD)
	cp -r $(STATIC) $(OUTPUT_FOLDER_WEB)

web: build-js html-web

web-dev: build-js-dev html-web

serve-web:
	cd $(OUTPUT_FOLDER_WEB) && \
	$(MAKEFILE_DIR)/node_modules/.bin/http-server \
		--cors \
		-p $(LOCAL_WEB_PORT) \
		-P "http://localhost:$(LOCAL_API_PORT)"

web-dev-watch:
	find .  -not -path "./node_modules/*" \
					-not -path "./jspm_packages/*" \
					-not -path "./target/*" \
					-not -path "./**/*.spec.js" \
					-type f \
					-regex ".*\.\(js\|css\|html\)$$" \
				 | entr -rc make web-dev serve-web

##############################
# Web Extension (FF, Chrome) #
##############################

web-extension-zip:
	mkdir -p $(OUTPUT_FOLDER_WEB_EXTENSION)
	cd $(OUTPUT_FOLDER_WEB) && zip -r ../../$(OUTPUT_FOLDER_WEB_EXTENSION)/$(WEB_EXTENSION_ZIP_NAME).zip .
	zip -j $(OUTPUT_FOLDER_WEB_EXTENSION)/$(WEB_EXTENSION_ZIP_NAME).zip $(WEB_EXTENSION_MANIFEST)

web-extension: build-js html-web web-extension-zip
web-extension-dev: build-js-dev html-web web-extension-zip web-extension-unpacked
web-extension-unpacked:
	unzip -o -qq $(OUTPUT_FOLDER_WEB_EXTENSION)/$(WEB_EXTENSION_ZIP_NAME).zip -d $(OUTPUT_FOLDER_WEB_EXTENSION)/$(WEB_EXTENSION_ZIP_NAME)-unpacked
web-extension-dev-watch:
	find .  -not -path "./node_modules/*" \
					-not -path "./jspm_packages/*" \
					-not -path "./target/*" \
					-not -path "./**/*.spec.js" \
					-type f \
					-regex ".*\.\(js\|css\|html\)$$" \
				 | entr -rc make web-extension-dev

###########
# Testing #
###########

test: unit-test integration-test e2e-test

unit-test:
	./node_modules/.bin/tape app/**/*.spec.js | ./node_modules/.bin/tap-min

unit-test-watch:
	find .  -not -path "./node_modules/*" \
					-not -path "./jspm_packages/*" \
					-not -path "./target/*" \
					-type f \
					-regex ".*\.\(js\|css\|html\)$$" \
				 | entr -rc make unit-test

e2e-test:
	./node_modules/.bin/tape app/**/*.spec.e2e.js | ./node_modules/.bin/tap-min

e2e-test-watch:
	find .  -not -path "./node_modules/*" \
					-not -path "./jspm_packages/*" \
					-not -path "./target/*" \
					-type f \
					-regex ".*\.\(js\|css\|html\)$$" \
				 | entr -rc make e2etest

##########
# Deploy #
##########

IMAGE_NAME=passcue-web
CONTAINER_NAME=passcue-web
DOCKER_HOST_IP := $(shell ip route | grep docker0 | awk '{print $$9}')

package: package-docker
package-docker: docker-container

docker-container:
	docker build -f infra/Dockerfile -t $(IMAGE_NAME) .

clean-docker-instance:
	docker rm $(CONTAINER_NAME)

docker-instance:
	docker run \
		--name $(CONTAINER_NAME) \
		-p 127.0.0.1:$(LOCAL_WEB_PORT):80 \
		-d $(IMAGE_NAME)

# Transfer the image to the host
docker-container-remote:
	@echo -e "Deploying container [$(IMAGE_NAME)] to host [$(SSH_ADDR)]"
	docker save $(IMAGE_NAME) | bzip2 | pv | ssh $(SSH_ADDR) 'bunzip2 | docker load'

# Deploy image on the host by getting the docker host ip then using it in the run command
docker-instance-remote:
	@echo -e "Connecting to host host [$(SSH_ADDR)] and starting container $(CONTAINER_NAME)..."
	ssh $(SSH_ADDR) "DOCKER_HOST_IP=`ip route | grep docker0 | awk '{print $$9}'`; \
			docker stop $(CONTAINER_NAME); \
			docker rm $(CONTAINER_NAME); \
		docker run \
		--name $(CONTAINER_NAME) \
		-p 127.0.0.1:$(LOCAL_WEB_PORT):80 \
		-d $(IMAGE_NAME)"

deploy: docker-container-remote docker-instance-remote

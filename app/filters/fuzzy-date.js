import _ from "lodash";
import moment from "moment";
import Vue from "vue";

const log = function() { console.log.apply(console, ["[fuzzy-date-filter]", ...arguments]); };

Vue.filter('fuzzy-date', function(date) {
  // Use moment to figure out the date
  var parsedDate = moment(date);
  if (!parsedDate.isValid() || !_.isDate(parsedDate.toDate())) {
    throw new Error(`Invalid date (${date}) passed to fuzzy-date filter, check logs`);
  }

  return parsedDate.calendar();
});

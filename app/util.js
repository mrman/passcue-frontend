import _ from "lodash";
import Constants from "app/constants";
import "fetch";

const DEFAULT_JSON_REQ_HEADERS = {
  "Content-Type": "application/json"
};

const EXPECTED_HINT_FIELDS = ["text", "url"];
const EXPECTED_NONEMPTY_HINT_FIELDS = EXPECTED_HINT_FIELDS;

/**
 * Performs a POST request to the API
 *
 * @param {string} baseURL - backend server address
 * @param {string} resourcePath - The path to the resource (not including base URL)
 * @param {Object} body - Body to be passed along as JSON
 * @param {Object} headers - Headers to add to the request (along with defaults)
 */
var postJSONToAPI = function(baseURL, resourcePath, body, headers) {
  var url = `${baseURL}/${Constants.URLS.API_BASE}/${Constants.URLS.API_VERSION}/${resourcePath}`;

  return fetch(url, {
    method: "POST",
    headers: _.extend(DEFAULT_JSON_REQ_HEADERS, headers || {}),
    body: JSON.stringify(body)
  });

};

/**
 * Performs a PUT request to the API
 *
 * @param {string} baseURL - backend server address
 * @param {string} resourcePath - The path to the resource (not including base URL)
 * @param {Object} body - Body to be passed along as JSON
 * @param {Object} headers - Headers to add to the request (along with defaults)
 */
var putJSONToAPI = function(baseURL, resourcePath, body, headers) {
  var url = `${baseURL}/${Constants.URLS.API_BASE}/${Constants.URLS.API_VERSION}/${resourcePath}`;

  return fetch(url, {
    method: "PUT",
    headers: _.extend(DEFAULT_JSON_REQ_HEADERS, headers || {}),
    body: JSON.stringify(body)
  });

};

/**
 * Performs a GET request to the API
 *
 * @param {string} baseURL - backend server address
 * @param {string} resourcePath - The path to the resource (not including base URL)
 * @param {Object} headers - Headers to add to the request (along with defaults)
 */
var getJSONFromAPI = function(baseURL, resourcePath, headers) {
  var url = `${baseURL}/${Constants.URLS.API_BASE}/${Constants.URLS.API_VERSION}/${resourcePath}`;

  return fetch(url, {
    method: "GET",
    headers: _.extend(DEFAULT_JSON_REQ_HEADERS, headers || {})
  });

};

/**
 * Performs a DELETE request to the API
 *
 * @param {string} baseURL - backend server address
 * @param {string} path - The path to the resource (not including base URL)
 * @param {Object} headers - Headers to add to the request (along with defaults)
 */
var deleteFromAPI = function(baseURL, resourcePath, headers) {
  var url = `${baseURL}/${Constants.URLS.API_BASE}/${Constants.URLS.API_VERSION}/${resourcePath}`;

  return fetch(url, {
    method: "DELETE",
    headers: _.extend(DEFAULT_JSON_REQ_HEADERS, headers || {})
  });

};


/**
   * Converts an object containing api key info into headers to be used with requests
   *
   * @param {Object} info - API key info
   * @param {string} info.key - API key
   * @param {string} info.secret - API secret
   * @returns an object containing auth headers to be used in a request
   */
var genAuthHeadersFromAPIKeyInfo = function(info) {
  return {
    "x-api-key": info.key,
    "x-api-secret": info.secret
  };
};

/**
 * Throws the provided error if a HTTP response is not 200
 *
 * @param {number[]} statuses - Valid/expected statues
 * @param {Error} error - The error that will be thrown
 * @returns A function that can be passed
 */
var errorOnUnexpectedStatus = function(statuses, err) {
  return function(res) {
    if (!_.includes(statuses, res.status)) {
      throw new Error(`Unexpected status ${res.status}, does not any expected statuses: [${statuses}]`);
    }
    return res;
  };
};

// Helper to jsonify fetch responses
var jsonifyFetchResponse = function(res) {
  return res.json();
};

/**
 * Validates whether an object is a valid hint
 *
 * @param {Object} obj - The object to check
 * @returns a bool representing whether the object is a valid hint
 */
var isValidHint = function(obj) {
  if (!_.isObject(obj)) return false;

  if (!_.every(EXPECTED_HINT_FIELDS, _.partial(_.has, obj))) return false;

  var hasUnexpectedEmptyField = _(EXPECTED_NONEMPTY_HINT_FIELDS)
      .map(_.partial(_.get, obj))
      .some(_.isEmpty);
  if (hasUnexpectedEmptyField) return false;

  return true;
};

/**
 * Checks whether both (valid) hints are the valid and the same,
 * will return false if either of the hints is invalid
 *
 * @param {Object} h1 - The first hint
 * @param {Object} h2 - The second hint
 * @returns a bool representing whether the hints are the same by content
 */
var hintsAreSame = function(h1, h2) {
  if (!isValidHint(h1) || !isValidHint(h2)) return false;

  // Short circuit, if they both have "id" set, which should be given by the server.
  if (_.has(h1, "id") && _.has(h2, "id")) {
    return h1.id === h2.id;
  }

  // Can't use created/id here becuase they're replaced server-side
  return h1.url === h2.url &&
    h1.text === h2.text;
};


var isUndefinedOrNull = function(obj) {
  return _.some([_.isUndefined, _.isNull], f => f(obj));
};
var isNullOrUndefined = isUndefinedOrNull;

// See https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
var generateUUID = function(){
  var d = new Date().getTime();

  var window; // Declare window just so tests don't barf
  if(window && window.performance && typeof window.performance.now === "function"){
    d += performance.now(); //use high-precision timer if available
  }

  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = (d + Math.random()*16)%16 | 0;
    d = Math.floor(d/16);
    return (c=='x' ? r : (r&0x3|0x8)).toString(16);
  });
  return uuid;
};

var inBrowserContext = function() {
  try {
    return !_.isUndefined(window);
  } catch (e) {
    return false;
  }
  return false;
};

/**
 * Get the hint url for user with the given ID
 *
 * @param {string} id - ID of the user for which to get hints URL for
 */
var getHintsUrlForUserWithId = function(id) {
  return [Constants.URLS.USERS, id, Constants.URLS.HINTS].join("/");
};

/**
 * Get the url for a single hint resource with the given user ID and hint ID
 *
 * @param {string} userId - ID of the user
 * @param {string} hintId - ID of the hint
 * @returns the resource path for the a specific hint for a specific user
 */
var getSingleHintUrlForUserWithId = function(userId, hintId) {
  return [Constants.URLS.USERS, userId, Constants.URLS.HINTS, hintId].join("/");
};

/**
 * Show alerts from a response envelope. This method is generally chained with a .then
 *
 * @param {Array} alerts - The alerts to which to add the created alert
 * @param {Object} [alertSvc] - If specified, the alert service to use
 * @param {string} [namespace] - The namespace of the alert, note alertSvc must be specified
 */
var showAlertFromResponseEnvelope = function(alerts, alertSvc, namespace) {
  if (isNullOrUndefined(alertSvc) || isNullOrUndefined(namespace)) {

    // Add an alert to the given list
    return function(res) {
      alerts.push({status: res.status, message: res.message});
      return res;
    };

  } else {

    // Use the given alert service
    return function(res) {
      alertSvc.addAlertForNamespace(namespace, {status: res.status, message: res.message});
    };

  }
};

export default {
  postJSONToAPI,
  putJSONToAPI,
  getJSONFromAPI,
  deleteFromAPI,
  genAuthHeadersFromAPIKeyInfo,
  errorOnUnexpectedStatus,
  jsonifyFetchResponse,
  isValidHint,
  hintsAreSame,
  isUndefinedOrNull,
  isNullOrUndefined,
  generateUUID,
  inBrowserContext,
  getHintsUrlForUserWithId,
  getSingleHintUrlForUserWithId,
  showAlertFromResponseEnvelope
};

import Vue from "vue";
import router from "vue-router";
import t from "./template.html!vtc";
import styles from "./style.css!";
import UserService from "app/services/user";
import AlertService from "app/services/alert";
import AppService from "app/services/app";
import EnvironmentService from "app/services/environment";
import AlertListing from "app/components/alert-listing/component";
import Util from "app/util";

const log = function() { console.log.apply(console, ["[signup-page]", ...arguments]); };

const comp = {
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  props: {
    initialEmail: String
  },

  data: function() {
    return {
      alerts: [],
      signupAlerts: AlertService.getAlertsForNamespace("signup-page"),
      appName: AppService.getAppName(),
      emailAddress: this.initialEmail || "",
      password: "",
      passwordConfirm: ""
    };
  },

  beforeCreate: function() {
    // Redirect to app immediately if UserService has an API key loaded
    log("Checking if the user is already logged in locally...");
    if (UserService.hasValidAPIKey()) {
      AppService.doNavigation(r => r.push('app'));
    }
  },

  methods: {

    validateSignupForm: function() {
      if (this.password !== this.passwordConfirm) {
        this.alerts.push({status: "error", message: "Passwords don't match!"});
        return false;
      }

      return true;
    },

    // Attempt to signup with the current username & password
    signup: function() {
      log(`Attempting sign up  for Passcue...`);
      var comp = this;

      // If form is valid, do signup
      if (this.validateSignupForm()) {

        UserService.signup(comp.emailAddress, comp.password)
          .then(Util.errorOnUnexpectedStatus([200, 401]))
          .then(Util.jsonifyFetchResponse)
          .then(Util.showAlertFromResponseEnvelope(comp.alerts))
          .catch(function(err) {
            log("Couldn't complete request, err:", err);
          });

      }

    }
  }
};

// Register and export component
Vue.component('signup-page', comp);
export default comp;

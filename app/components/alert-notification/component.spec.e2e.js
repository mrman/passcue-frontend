var should = require('should');
var TestUtil = require('../../../test/util');
var fixtures = require('../../../test/fixtures/alerts.js');
var test = TestUtil.tape;

var COMPONENT_JS_PATH = 'app/components/alert-notification/component.js';

test("alert-notification component should $emit a destroy event when the x is clicked", {timeout: 10000}, t => {

  TestUtil.renderModuleInJSDOM(COMPONENT_JS_PATH, "Component")
    .then(TestUtil.instantiateComponentFromLoadedGlobal("Component", fixtures.ERROR))
    .then(elemInfo => {

      // Watch for destroy to be emitted
      elemInfo.vm.$on('destroy', (alert, alertIdx) => {
        t.pass("destroy event emitted");

        // Ensure that the $emit-ed destroy contains the alert and alert ID we expect
        t.assert( alert.should.be.deepEqual(fixtures.ERROR.propsData.alert), "alerts hould be same as fixture data");
        t.assert( alertIdx.should.be.eql(fixtures.ERROR.propsData.alertIdx), "alert index should be the same as fixture data");

        t.end();
      });

      // Click on the X, ensure it triggers a destroy,
      TestUtil.jsdomClick(elemInfo.elem.firstChild, elemInfo.window);
    })
    .catch(TestUtil.failAndEnd(t));

});

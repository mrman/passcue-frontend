import Vue from "vue";
import t from "./template.html!vtc";
import styles from "./style.css!";
import _ from "lodash";

const comp = {
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  props: {
    alert: Object,
    alertIdx: Number
  },

  data: function() {
    return {
      alert: this.alert,
      alertIdx: this.alertIdx
    };
  },

  computed: {
    alertClass: function() {
      var statusClass = _.has(this.alert, "status") ? `alert-${this.alert.status}` : "";
      return ["alert", statusClass].join(" ");
    }
  },

  methods: {
    log: function() { console.log.apply(console, ["[alert-notification]", ...arguments]); },

    // Destroy this alert (emits event to parent)
    destroy: function() {
      this.$emit('destroy', this.alert, this.alertIdx);
    }
  }
};

// Register as global component & export
Vue.component('alert-notification', comp);

// Expose the component and the vue instance used to create it (useful for testing)
export default { component: comp, vue: Vue };

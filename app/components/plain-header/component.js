import Vue from "vue";
import t from "./template.html!vtc";

const log = function() { console.log.apply(console, ["[plain-header]", ...arguments]); };

const comp = {
  render: t.render,
  staticRenderFns: t.staticRenderFns,
  props: {
    showBackButton: Boolean
  }
};

// Register and export component
Vue.component('plain-header', comp);
export default {
  vue: Vue,
  component: comp
};

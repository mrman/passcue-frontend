import _ from "lodash";
import Vue from "vue";
import AlertNotification from "app/components/alert-notification/component";
import t from "./template.html!vtc";

const log = function() { console.log.apply(console, ["[alert-listing]", ...arguments]); };

const comp = {
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  props: {
    alerts: _.isList,
    maxAlertCount: {
      validator: function(c) {
        return _.isNumber(c) && c > 0;
      }
    }
  },

  data: function() {
    return {
      alerts: this.alerts,
      maxAlertCount: this.maxAlertCount || -1
    };
  },

  computed: {
    processedAlerts: function() {
      // If maxAlertCount is specified, remove all others (till max is reached)
      // This assumes that new entries are PUSHED.
      while (this.maxAlertCount != -1 && this.alerts.length > this.maxAlertCount) {
        this.alerts.splice(0, 1);
      }

      return this.alerts;
    }

  },

  methods: {
    // Remove an alert
    destroyAlert: function(alert, idx) {
      log(`Destroying matching alert with index ${idx}`);
      var a = this.alerts[idx];

      if (a.status === alert.status &&
          a.message === alert.message) {
        this.alerts.splice(idx, 1);
      }

    }
  }
};

// Register and export component
Vue.component('alert-listing', comp);
export default comp;

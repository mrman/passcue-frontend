import _ from "lodash";
import Vue from "vue";
import HintService from "app/services/hint";
import HintListItem from "app/components/hint-list-item/component";
import t from "./template.html!vtc";
import style from "./style.css!";
import Util from "app/util";

const log = function() { console.log.apply(console, ["[hint-listing]", ...arguments]); };

const FILTER_FIELDS = ["text", "url"];

const comp = {
  render: t.render,
  staticRenderFns: t.staticRenderFns,
  filters: Util.VueFilters,

  props: {
    filterInput: String,
    staticHints: Array
  },

  data: function() {
    var hints = this.staticHints ? this.staticHints : HintService.getHints();
    var filterInput = this.filterInput || "";

    return {
      filterInput: filterInput,
      hints: hints,
      filterTerm: filterInput,

      // Services
      svc: {
        hint: HintService
      }
    };
  },

  watch: {

    filterInput: function() {
      this.setFilterTerm();
    }

  },

  methods: {

    setFilterTerm: _.throttle(function() {
      this.filterTerm = this.filterInput;
    }, 500, {trailing: true}),

    bubbleRemoveHint: function() {
      this.$emit.apply(this, ['remove-hint', ...arguments]);
    }

  },

  computed: {

    filteredHints: function() {
      // If filter term is invalid or empty don't filter
      if (!_.isString(this.filterTerm) || _.isEmpty(this.filterTerm)) { return this.hints; }

      var term = this.filterTerm;
      return _.filter(this.hints, h => {
        return _(FILTER_FIELDS)
        // Get the fields we check
          .map(_.partial(_.get, h))
        // Check if at least one field contains the term
          .some(v => _.includes(v, term));
      });
    }

  }

};

// Register and export component
Vue.component('hint-listing', comp);

// Expose the component and the vue instance used to create it (useful for testing)
export default { component: comp, vue: Vue };

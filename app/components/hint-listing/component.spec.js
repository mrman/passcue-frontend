var should = require('should');
var TestUtil = require('../../../test/util');
var fixtures = require('../../../test/fixtures/hint-listing.js');
var JSPM = require('jspm');
var test = TestUtil.tape;

// Define the systemjs path to the component code
var COMPONENT_JS_PATH = 'app/components/hint-listing/component.js';

var Vue, Component;

test("hint-listing component", t => {

  // Import the component, using JSPM for uinit tests
  JSPM.import(COMPONENT_JS_PATH)
    .then(module => {
      t.ok(module.default.vue);
      t.ok(module.default.component);

      // Save Vue and component class to test-globals for later
      Vue = module.default.vue;
      Component = module.default.component;
    })
    .then(t.end)
    .catch(err => {
      console.log("Error occurred during setup:", err);
      t.fail(err);
    });
});

test("hint-listing component data is a function", t => {
  t.plan(1);
  t.assert( Component.data.should.have.type('function'), "Component data should be a function");
});

test("hint-listing component data spits out the correct fields and default values", t => {
  t.plan(4);

  var data = Component.data();
  t.assert( data.should.have.type('object'),  "Data function should return an object");
  t.assert( data.filterInput.should.be.empty(), "Data.filterInput should be empty initially");
  t.assert( data.hints.should.be.empty(), "Data.hints should be empty initially");
  t.assert( data.filterTerm.should.be.empty(), "Data.filterTerm should be empty intiially");
});

test("hint-listing component has the default data values after instantiation (with no params)", t => {
  t.plan(2);

  var vm = new Vue(Component).$mount();
  t.assert( should(vm.filterInput).be.undefined(),
            "When not passed any input, data.filterInput should be undefined");
  t.assert( vm.hints.should.be.empty(), "When not passed any input, data.hints should be empty initially");
});

test("hint-listing component properly server-side renders", t => {

  TestUtil.renderComponentWithData(Vue, Component)
    .then(w => {

      // Get the element and context
      var e = w.document.getElementsByClassName("hint-listing-component")[0];
      t.ok(e, "Element should be present");
    })
    .then(t.end)
    .catch(TestUtil.failAndEnd(t));
});

test("hint-listing component has passed in (static) hints on render", t => {

  TestUtil.renderComponentWithData(Vue, Component, fixtures.STATIC_HINTS)
    .then(w => {

      // Get the element and context
      var e = w.document.getElementsByClassName("hint-listing-component")[0];
      t.ok(e, "Element should be present");

      var expectedNumHints = fixtures.STATIC_HINTS.propsData.staticHints.length;
      var hintElems = Array.from(w.document.getElementsByClassName("hint-listing")[0].children);
      t.assert( hintElems.should.not.be.empty(), "Hint listing UL (.hint-listing) should not be empty");
      t.assert( hintElems.should.have.length(expectedNumHints), "all hints should be shown");

    })
    .then(t.end)
    .catch(TestUtil.failAndEnd(t));
});

test("hint-listing component renders right hints when filterInput is passed in", t => {

  TestUtil.renderComponentWithData(Vue, Component, fixtures.STATIC_HINTS_WITH_FILTER)
    .then(w => {

      // Get the element and context
      var e = w.document.getElementsByClassName("hint-listing-component")[0];
      t.ok(e, "Element should be present");

      var hintElems = Array.from(w.document.getElementsByClassName("hint-listing")[0].children);
      t.assert( hintElems.should.have.length(1), "Only the google hint should be shown");

    })
    .then(t.end)
    .catch(TestUtil.failAndEnd(t));
});

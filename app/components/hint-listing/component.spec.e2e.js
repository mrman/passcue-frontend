var should = require('should');
var TestUtil = require('../../../test/util');
var fixtures = require('../../../test/fixtures/hint-listing.js');
var test = TestUtil.tape;

var COMPONENT_JS_PATH = 'app/components/hint-listing/component.js';

test("component should properly render off page", {timeout: 10000}, t => {

  TestUtil.renderModuleInJSDOM(COMPONENT_JS_PATH, "Component")
    .then(TestUtil.instantiateComponentFromLoadedGlobal("Component"))
    .then(TestUtil.addComponentElementToBody)
    .then(elemInfo => {
      // Ensure element is present
      t.ok(elemInfo.elem, "Element was properly rendered");
    })
    .then(t.end)
    .catch(TestUtil.failAndEnd(t));

});

test("component should bubble the delete of an list item", {timeout: 10000}, t => {

  TestUtil.renderModuleInJSDOM(COMPONENT_JS_PATH, "Component")
    .then(TestUtil.instantiateComponentFromLoadedGlobal("Component", fixtures.STATIC_HINTS))
    .then(TestUtil.addComponentElementToBody)
    .then(elemInfo => {
      t.ok(elemInfo.elem, "Element was properly rendered");

      // Get the first hint
      var firstHintContainer = elemInfo.elem.getElementsByClassName("hint-listing")[0].firstChild;
      t.ok(firstHintContainer, "First hint should be present");

      // Get the delete button (trashcan) on the first hint
      var deleteBtn = firstHintContainer.getElementsByClassName("fa-trash")[0];
      t.ok(deleteBtn, "Trashcan Icon on first hint should be present");

      // Watch for destroy to be emitted
      elemInfo.vm.$on('remove-hint', hint => {
        t.pass("remove-hint event bubbled");

        // Ensure that the $emit-ed remove-hint contains the hint we expect
        t.deepEqual( fixtures.STATIC_HINTS.propsData.staticHints[0], hint, "hint should match first hint in fixture data");

        t.end();
      });

      // Click on the trashcan icon, ensure it triggers a remove-hint
      TestUtil.jsdomClick(deleteBtn, elemInfo.window);

    })
    .catch(TestUtil.failAndEnd(t));

});

test("component should filter hints properly when passed static hints and filter", {timeout: 10000}, t => {

  TestUtil.renderModuleInJSDOM(COMPONENT_JS_PATH, "Component")
    .then(TestUtil.instantiateComponentFromLoadedGlobal("Component", fixtures.STATIC_HINTS_WITH_FILTER))
    .then(TestUtil.addComponentElementToBody)
    .then(elemInfo => {
      // Put item on page
      t.ok(elemInfo.elem, "Element was properly rendered");

      // Get the <ul> that contains all the hints
      var hintListingUL = elemInfo.elem.getElementsByClassName("hint-listing")[0];
      t.ok(hintListingUL, "Hint listing should be present");

      // Ensure that the hints
      var hintContainers = Array.from(hintListingUL.children);
      t.assert( hintContainers.should.not.be.empty(), "Hints should be shown");
      t.assert( hintContainers.should.have.length(1), "Only one hint should be shown");

    })
    .then(t.end)
    .catch(TestUtil.failAndEnd(t));

});

test("component's hints should change if HintService's list of hints change", {timeout: 10000}, t => {

  TestUtil.renderModuleInJSDOM(COMPONENT_JS_PATH, "Component")
    .then(TestUtil.instantiateComponentFromLoadedGlobal("Component"))
    .then(TestUtil.addComponentElementToBody)
    .then(elemInfo => {
      t.ok(elemInfo.elem, "Element was properly rendered");

      // Get the <ul> that contains all the hints, ensure it is empty
      var hintListingUL = elemInfo.elem.getElementsByClassName("hint-listing")[0];
      t.ok(hintListingUL, "Hint listing should be present");
      t.assert( Array.from(hintListingUL.children).should.be.empty(), "no hints should be shown initially");

      // Get the HintService
      var hintSvc = elemInfo.vm.svc.hint;
      t.ok(hintSvc, "HintService should be present");
      
      // Add a hint to the hint service, then ensure it gets added, etc
      hintSvc
        .addHint(hintSvc.createHint("google.com", "this is a test hint"))
        .then(h => {
          // Ensure hint was returned by promise
          t.ok(h);

          // Ensure hint service reflects
          t.assert( hintSvc.getHints().length.should.be.eql(1), "hint should have been added to hint service");

          // Check that the hint is on the page
          t.assert( Array.from(hintListingUL.children).should.have.length(1), "hint showing on page");
        })
        .then(t.end)
        .catch(TestUtil.failAndEnd(t, "adding hint should not reject"));

    })
    .catch(TestUtil.failAndEnd(t));

});

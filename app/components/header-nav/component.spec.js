var should = require('should');
var JSPM = require('jspm');
var TestUtil = require('../../../test/util');
var test = TestUtil.tape;

// Define the systemjs path to the component code
var COMPONENT_JS_PATH = 'app/components/header-nav/component.js';

// Setup
var Vue, Component;

test("header-nav component", t => {

  // Import the component, using JSPM for uinit tests
  JSPM.import(COMPONENT_JS_PATH)
    .then(module => {
      t.ok(module.default.vue);
      t.ok(module.default.component);

      // Save Vue and component class to test-globals for later
      Vue = module.default.vue;
      Component = module.default.component;
    })
    .then(t.end)
    .catch(err => {
      console.log("Error occurred during done:", err);
      t.fail(err);
    });
});

test("header-nav component is the correct type", t => {
  t.plan(1);

  t.assert( Component.data.should.have.type('function'), "Component.data should be of type function");
});

test("header-nav component has the correct fields and default values", t => {
  t.plan(3);

  var data = Component.data();

  t.assert( data.should.have.type('object'), "Data function should return an object");
  t.assert( data.appName.should.eql("Passcue"), "appName should be 'Passcue'");
  t.assert( data.headerAlerts.should.be.empty(), "headerAlerts should start off as empty");
});

test("header-nav component has the default data values after instantiation (with no params)", t => {
  t.plan(2);

  var vm = new Vue(Component).$mount();
  t.assert( vm.appName.should.eql("Passcue"), "appName should be Passcue");
  t.assert( vm.headerAlerts.should.be.empty(), "headerAlerts should be empty");
});

test("header-nav component should server-side render properly", t => {

  TestUtil.renderComponentWithData(Vue, Component)
    .then(w => {

      // Get the element
      var e = w.document.getElementsByClassName("header-nav-component")[0];

      t.assert( e.should.not.be.undefined(), ".header-nav-component element should exist");

      // Expect certain elements to be present
      t.assert( w.document.getElementById("home-link").should.not.be.null(), "#home-link should be present");
      t.assert( w.document.getElementById("settings-link").should.not.be.null(), "#settings-link should be present");
      t.assert( w.document.getElementById("create-hint-nav-button").should.not.be.null(), "#create-hint-nav-button should be present");
      t.assert( w.document.getElementById("refresh-hints-nav-button").should.not.be.null(), "#refresh-hints-nav-button should be present");
      t.assert( w.document.getElementById("sign-out-nav-button").should.not.be.null(), "#sign-out-nav-button should be present");

    })
    .then(t.end)
    .catch(TestUtil.failAndEnd(t));

});

import _ from "lodash";
import Vue from "vue";
import AlertService from "app/services/alert";
import AppService from "app/services/app";
import AlertListing from "app/components/alert-listing/component";
import style from "./style.css!";
import t from "./template.html!vtc";

const log = function() { console.log.apply(console, ["[header-nav]", ...arguments]); };

const comp = {
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  data: function() {
    return {
      headerAlerts: AlertService.getAlertsForNamespace("header"),
      appName: AppService.getAppName() || null
    };
  }
};

// Register and export component
Vue.component('header-nav', comp);
export default {
  vue: Vue,
  component: comp
};

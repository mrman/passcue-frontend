var should = require('should');
var TestUtil = require('../../../test/util');
var test = TestUtil.tape;

var COMPONENT_JS_PATH = 'app/components/header-nav/component.js';

const EXPECTED_LINKS = [
  "Settings",
  "New Hint",
  "Refresh",
  "Sign out"
];

test("header-nav component should contain the proper links", {timeout: 10000}, function(t) {
  t.plan(3);

  // Render module in JSDOM
  TestUtil.renderModuleInJSDOM(COMPONENT_JS_PATH, "Component")
    .then(TestUtil.instantiateComponentFromLoadedGlobal("Component"))
    .then(elemInfo => {

      // Get all the links
      var links = elemInfo.elem.getElementsByClassName("pure-menu-link");
      t.notEqual(links.length, 0, "Elements with class 'pure-menu-link' should be present");

      // Get text for available links
      var trimmedLinkText = [];
      for (var i = 0; i < links.length; i++) {
        trimmedLinkText.push(links.item(i).textContent.trim());
      }

      // Ensure expected link text is present
      t.assert( trimmedLinkText.should.not.be.empty(), "trimmed link text should not be empty");
      t.assert( trimmedLinkText.should.containDeep(EXPECTED_LINKS), "trimmed link text should contain the links we expect");

    })
    .catch(TestUtil.failAndEnd(t));

});

test("header-nav component should $emit a hint-create when new hint button is clicked", {timeout: 10000}, function(t) {

  TestUtil.renderModuleInJSDOM(COMPONENT_JS_PATH, "Component")
    .then(TestUtil.instantiateComponentFromLoadedGlobal("Component"))
    .then(elemInfo => {

      // Watch for hint-create to be emitted
      elemInfo.vm.$on('hint-create', function() {
        t.pass("Emitted hint-create event");
        t.end();
      });

      // Append created header-nav component element to body
      var bodyElem = elemInfo.window.document.getElementsByTagName("body")[0];
      bodyElem.appendChild(elemInfo.elem);

      // Get all the links
      var links = elemInfo.elem.getElementsByClassName("pure-menu-link");
      t.notEqual(links.length, 0, "Elements with class 'pure-menu-link' should be present");

      // Get the new hint button
      var btn;
      for (var i = 0; i < links.length; i++) {
        var link = links.item(i);
        if (link.textContent.trim() === "New Hint") {
          btn = link;
          break;
        };
      }

      t.assert( btn.should.not.be.undefined(), "The new hint button should be present");

      // Click the new hint button
      t.comment("Clicking the new hint button...");
      TestUtil.jsdomClick(btn, elemInfo.window);

    })
    .catch(TestUtil.failAndEnd(t));

});

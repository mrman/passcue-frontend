import Vue from "vue";
import t from "./template.html!vtc";
import styles from "./style.css!";
import FuzzyDateFilter from "app/filters/fuzzy-date";
import HintIcon from "app/components/hint-icon/component";
import Util from "app/util";
import Constants from "app/constants";
import _ from "lodash";

var log = function() { console.log.apply(console, ["[hint-list-item]", ...arguments]); };

const comp = {
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  props: {
    hint: {
      validator: Util.isValidHint
    }
  },

  data: function() {
    return {
      hint: this.hint || null
    };
  },

  computed: {
    companyClass: function() {
      // Return nothing if hint is undefined/null/empty
      if (Util.isUndefinedOrNull(this.hint)) return "";

      var m = _.find(Constants.URL_TERM_TO_ICON_CLASS, pair => _.includes(this.hint.url, pair[0]));
      var cls = _.nth(m, 1);
      return `${ cls ? cls.replace('fa-','') : ""}`;
    }


  }

};

// Register and export component
Vue.component('hint-list-item', comp);

// Expose the component and the vue instance used to create it (useful for testing)
export default { component: comp, vue: Vue };

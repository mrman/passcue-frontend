var should = require('should');
var TestUtil = require('../../../test/util');
var fixtures = require('../../../test/fixtures/hint-list-item.js');
var test = TestUtil.tape;

var COMPONENT_JS_PATH = 'app/components/hint-list-item/component.js';

test("component should $emit a remove-hint event when trashcan is clicked", {timeout: 10000}, t => {

  TestUtil.renderModuleInJSDOM(COMPONENT_JS_PATH, "Component")
    .then(TestUtil.instantiateComponentFromLoadedGlobal("Component", fixtures.HN_HINT))
    .then(TestUtil.addComponentElementToBody)
    .then(elemInfo => {

      // Get trash icon container and icon
      var container = elemInfo.elem.getElementsByClassName("pull-right")[0];
      t.ok(container, "Trash can icon container should be present");

      var trashIcon = container.firstChild;
      t.ok(trashIcon, "Trash can icon should be present");

      // Watch for destroy to be emitted
      elemInfo.vm.$on('remove-hint', hint => {
        t.pass("remove-hint event emitted");

        // Ensure that the $emit-ed remove-hint contains the hint we expect
        t.deepEqual( fixtures.HN_HINT.propsData.hint, hint, "hint should match fixture data");

        t.end();
      });

      // Click on the trashcan icon, ensure it triggers a remove-hint
      TestUtil.jsdomClick(trashIcon, elemInfo.window);
    })
    .catch(TestUtil.failAndEnd(t));

});

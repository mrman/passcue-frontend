var should = require('should');
var TestUtil = require('../../../test/util');
var fixtures = require('../../../test/fixtures/hint-list-item.js');
var JSPM = require('jspm');
var test = TestUtil.tape;

// Define the systemjs path to the component code
var COMPONENT_JS_PATH = 'app/components/hint-list-item/component.js';

var Vue, Component;

test("hint-list-item component", t => {

  // Import the component, using JSPM for uinit tests
  JSPM.import(COMPONENT_JS_PATH)
    .then(module => {
      t.ok(module.default.vue);
      t.ok(module.default.component);

      // Save Vue and component class to test-globals for later
      Vue = module.default.vue;
      Component = module.default.component;
    })
    .then(t.end)
    .catch(err => {
      console.log("Error occurred during setup:", err);
      t.fail(err);
    });
});

test("hint-list-item component data is a function", t => {
  t.plan(1);
  t.assert( Component.data.should.have.type('function'), "Component data should be a function");
});

test("hint-list-item component data spits out the correct fields and default values", t => {
  t.plan(2);

  var data = Component.data();
  t.assert( data.should.have.type('object'),  "Data function should return an object");
  t.assert( should(data.hint).be.null(), "Data.hint should be null initially");
});

test("hint-list-item component has the default data values after instantiation (with no params)", t => {
  t.plan(1);

  var vm = new Vue(Component).$mount();
  t.assert( should(vm.hint).be.undefined(), "When not passed a hint, data.hint should be undefined");
});

test("hint-list-item component properly server-side renders a the right hackernews company header", t => {

  TestUtil.renderComponentWithData(Vue, Component, fixtures.HN_HINT)
    .then(w => {

      // Get the element and context
      var e = w.document.getElementsByClassName("hint-list-item-component")[0];
      var classes = Array.prototype.slice.call(e.classList);

      // Ensure expected hint related elements are showing up
      var iconElem = e.getElementsByClassName("url-container")[0].firstChild;
      t.ok(iconElem, "The hint icon should show up");

      // Ensure expected hint related elements are showing up
      var content = e.getElementsByClassName("content")[0];
      var contentText = content.textContent;
      t.ok(contentText, "The hint content section should show up");

      var createdDateElem = content.getElementsByClassName("hint-created-date");
      t.ok(createdDateElem, "Created date should show");

      // Ensure expected classes
      t.assert( e.tagName.should.eql("DIV"), "element should be an <div>");
      t.assert( classes.should.containEql('hint-list-item-component'), "component should have proper class");
      t.assert( classes.should.containEql('y-combinator'), "component should have proper company class");
      t.assert( contentText.should.containEql(fixtures.HN_HINT.propsData.hint.text),
                "list item should show hint text");
    })
    .then(t.end)
    .catch(TestUtil.failAndEnd(t));
});

import Vue from "vue";
import router from "vue-router";
import t from "./template.html!vtc";
import styles from "./style.css!";
import UserService from "app/services/user";
import AlertService from "app/services/alert";
import AppService from "app/services/app";
import EnvironmentService from "app/services/environment";
import AlertListing from "app/components/alert-listing/component";
import Util from "app/util";

const log = function() { console.log.apply(console, ["[login-page]", ...arguments]); };

const comp = {
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  data: function() {
    // Automatically show advanced options if the env service has a server base url set
    var showAdvancedOptions = EnvironmentService.state.serverBaseURL ? true : false;

    return {
      alerts: [],
      loginAlerts: AlertService.getAlertsForNamespace("login"),
      appName: AppService.state.appName,
      email: "",
      password: "",
      showPlainTextPassword: false,
      showAdvancedOptions,
      serverBaseURL: ""
    };
  },

  beforeCreate: function() {
    // Redirect to app immediately if UserService has an API key loaded
    log("Checking if the user is already logged in locally...");
    if (UserService.hasValidAPIKey()) {
      AppService.doNavigation(r => r.push('app'));
    }
  },

  // TODO: refactor this, should be able to just have a settings text widget
  computed: {
    serverBaseURLMatchesEnvSvc: function() {
      return EnvironmentService.state.serverBaseURL === this.serverBaseURL;
    },

    serverBaseURLBtnClass: function() {
      return this.serverBaseURLMatchesEnvSvc ? "button-success" : "button-error";
    },

    serverBaseURLBtnIconClass: function() {
      return this.serverBaseURLMatchesEnvSvc ? "fa-check" : "fa-save";
    }

  },

  methods: {

    // Save the local server base URL advanced option with the environment service
    saveServerBaseURLToEnvSvc: function() {
      EnvironmentService.setServerBaseURL(this.serverBaseURL);
    },

    saveAdvancedOptions: function() {
      this.saveServerBaseURLToEnvSvc();
    },

    // Attempt to login with the current username & password
    login: function() {
      log(`Attempting log in to Passcue...`);
      var comp = this;

      UserService.login(comp.email, comp.password)
        .then(Util.errorOnUnexpectedStatus([200, 401]))
        .then(Util.jsonifyFetchResponse)
        .then(function(res) {
          // Add alert
          comp.alerts.push({status: res.status, message: res.message});

          // Clear app alerts (if there are any)
          AlertService.clearAlerts();

          if (res.status === "success") {
            log(`Successful login with API key ${res.data.apiKey.key}, redirecting to app page..`);

            // Set the API key on the user service, and re-direct to app
            setTimeout(function() {
              UserService
                .setAPIKeyInfo(res.data.apiKey)
                .then((() => AppService.doNavigation(r => r.push('app'))));
            }, 1500); // Give the user some time to see the success message
          }

        })
        .catch(function(err) {
          log("Couldn't complete request, err:", err);
        });
    }
  }
};

// Register and export component
Vue.component('login-page', comp);
export default comp;

import _ from "lodash";
import Vue from "vue";
import t from "./template.html!vtc";
import Constants from "app/constants";

var log = function() { console.log.apply(console, ["[hint-icon]", ...arguments]); };

const comp = {
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  props: {
    url: String
  },

  data: function() {
    return {
      url: this.url || ""
    };
  },

  computed: {

    iconClass: function() {
      var m = _.find(Constants.URL_TERM_TO_ICON_CLASS, pair => _.includes(this.url, pair[0]));
      return `fa ${ _.nth(m, 1) || "fa-globe" }`;
    }

  }

};

// Register and export component
Vue.component('hint-icon', comp);

// Expose the component and the vue instance used to create it (useful for testing)
export default { component: comp, vue: Vue };

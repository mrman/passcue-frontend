var should = require('should');
var TestUtil = require('../../../test/util');
var fixtures = require('../../../test/fixtures/hint-icon.js');
var JSPM = require('jspm');
var test = TestUtil.tape;

// Define the systemjs path to the component code
var COMPONENT_JS_PATH = 'app/components/hint-icon/component.js';

var Vue, Component;

test("hint-icon component", t => {

  // Import the component, using JSPM for uinit tests
  JSPM.import(COMPONENT_JS_PATH)
    .then(module => {
      t.ok(module.default.vue);
      t.ok(module.default.component);

      // Save Vue and component class to test-globals for later
      Vue = module.default.vue;
      Component = module.default.component;
    })
    .then(t.end)
    .catch(err => {
      console.log("Error occurred during setup:", err);
      t.fail(err);
    });
});

test("hint-icon component data is a function", t => {
  t.plan(1);
  t.assert( Component.data.should.have.type('function'), "Component data should be a function");
});

test("hint-icon component data spits out the correct fields and default values", t => {
  t.plan(2);

  var data = Component.data();
  t.assert( data.should.have.type('object'),  "Data function should return an object");
  t.assert( should(data.url).be.empty(), "Data.url should be empty initially (not undefined)");
});

test("hint-icon component has the default data values after instantiation (with no params)", t => {
  t.plan(1);

  var vm = new Vue(Component).$mount();
  t.assert( should(vm.url).be.undefined(), "When not passed a prop, data.url should be undefined");
});

test("hint-icon component properly server-side renders a the right hackernews icon", t => {

  TestUtil.renderComponentWithData(Vue, Component, fixtures.HN_HINT)
    .then(w => {

      // Get the element and context
      var e = w.document.getElementsByClassName("hint-icon-component")[0];
      var classes = Array.prototype.slice.call(e.classList);

      // Ensure expected classes
      t.assert( e.tagName.should.eql("I"), "element should be an <i>");
      t.assert( classes.should.containEql('fa'), "'fa' should be in the class list");
      t.assert( classes.should.containEql('fa-y-combinator'), "'fa-y-combinator' should be in the class list");

    })
    .then(t.end)
    .catch(TestUtil.failAndEnd(t));
});

test("hint-icon component properly server-side renders a the right unknown site icon", t => {

  TestUtil.renderComponentWithData(Vue, Component, fixtures.UNKNOWN_SITE_HINT)
    .then(w => {

      // Get the element and context
      var e = w.document.getElementsByClassName("hint-icon-component")[0];
      var classes = Array.prototype.slice.call(e.classList);

      // Ensure expected classes
      t.assert( e.tagName.should.eql("I"), "element should be an <i>");
      t.assert( classes.should.containEql('fa'), "'fa' should be in the class list");
      t.assert( classes.should.containEql('fa-globe'), "'fa-globe' should be in the class list");

    })
    .then(t.end)
    .catch(TestUtil.failAndEnd(t));
});

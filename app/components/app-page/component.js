import Vue from "vue";
import t from "./template.html!vtc";
import UserService from "app/services/user";
import AlertService from "app/services/alert";
import AppService from "app/services/app";
import HintService from "app/services/hint";
import AlertListing from "app/components/alert-listing/component";
import HeaderNav from "app/components/header-nav/component";
import FooterNav from "app/components/footer-nav/component";
import HintListing from "app/components/hint-listing/component";
import HintCreationDialog from "app/components/hint-creation-dialog/component";
import Util from "app/util";
import style from "./style.css!";

const log = function() { console.log.apply(console, ["[app-page]", ...arguments]); };

const comp = {
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  beforeCreate: function() {
    // Redirect to login immediately if UserService does not have a valid API key
    log("Ensuring UserService has a valid API Key...");
    if (!UserService.hasValidAPIKey()) {
      AppService.doNavigation(r => r.push('login'));
    }
  },

  data: function() {
    return {
      hintCreationDialogShown: false,
      filterInput: "",
      currentUserAPIKey: UserService.getAPIKey() || null
    };
  },

  methods: {
    doLogout: function() {

      // Attempt to log out the current user
      UserService
        .logout()
        .then(user => {
          log("Logged out user...");

          // Show logout alert
          AlertService.addAlertForNamespace({
            status: "success",
            message: "Successfully logged out"
          }, "login");

          // Redirect to login page
          AppService.doNavigation(r => r.push('login'));
        })
        .catch(err => {
          // TODO: Show alert about failing to logout
        });

    },

    refreshHints: function() {
      HintService.fetchHints();
    },

    // Save a created hint to the hint service
    saveCreatedHint: HintService.addHint,

    removeHint: HintService.removeHint

  }
};

// Register and export component
Vue.component('app-page', comp);
export default comp;

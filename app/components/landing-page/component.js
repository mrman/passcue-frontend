import Vue from "vue";
import t from "./template.html!vtc";
import styles from "./style.css!";
import AppService from "app/services/app";
import UserService from "app/services/user";
import EnvironmentService from "app/services/environment";
import Constants from "app/constants";

const comp = {
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  beforeCreate: function() {
    // On extension contexts, skip to app apge if login is present
    var currentEnv = EnvironmentService.envName;
    var inExtensionEnv = [Constants.ENV_NAMES.CHROME_EXTENSION,
                          Constants.ENV_NAMES.FIREFOX_EXTENSION].includes(currentEnv);

    if (inExtensionEnv && UserService.hasValidAPIKey()) {
      AppService.doNavigation(r => r.push('app'));
    }
  },

  data: function() {
    return {
      appName: AppService.state.appName,
      currentYear: new Date().getFullYear(),
      emailAddress: ""
    };
  },

  methods: {

    // Redirect the user on the landing page to registration page with their email address
    redirectToSignupWithEmail: function() {
      AppService.doNavigation(r => r.push({name: 'signup',
                                           query: { emailAddress: this.emailAddress }}) );
    }

  }
};

// Register and export component
Vue.component('landing-page', comp);
export default comp;

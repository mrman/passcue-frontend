import Vue from "vue";
import t from "./template.html!vtc";
import HintService from "app/services/hint";
import AlertService from "app/services/alert";
import Util from "app/util";

var log = function() { console.log.apply(console, ["[hint-creation-dialog]", ...arguments]); };

const HINT_CREATION_ALERT_NAMESPACE = "hint-creation";

const comp = {
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  props: {
    saveCreatedHintFn: Function
  },

  data: function() {
    return {
      alerts: AlertService.getAlertsForNamespace(HINT_CREATION_ALERT_NAMESPACE),
      savingHintMessageShown: false,
      url: "",
      text: ""
    };
  },

  methods: {

    reset: function() {
      this.url = "";
      this.text = "";
      this.savingHintMessageShown = false;
    },

    createHint: function() {
      var hint = HintService.createHint(this.url, this.text);

      // Show alert & return early if hint is invalid
      if (!Util.isValidHint(hint)) {
        AlertService.addAlertForNamespace({
          status: "error",
          message: "Invalid hint, please ensure that the hint text and URL are valid."
        }, HINT_CREATION_ALERT_NAMESPACE);

        return;
      }

      // If a function to call wasn't provided, simply emit and exit early
      if (Util.isNullOrUndefined(this.saveCreatedHintFn)) {
        this.$emit('hint-created', hint);
        return;
      }

      // Before starting to save the hint, show saving message
      // Normally, saving should be so fast that user won't even see this.
      this.savingHintMessageShown = true;

      // Save the created hint using provided function, handle UI changes
      this.saveCreatedHintFn(hint)
        .then(h => {
          this.saveCreatedHintFn = false;
          this.$emit('hide');

          // Reset form
          this.reset();
        })
        .catch(err => {
          this.saveCreatedHintFn = false;

          // Show error if hint creation failed
          AlertService.addAlertForNamespace({
            status: "error",
            message: "Hint creation failed... Please try again later."
          }, HINT_CREATION_ALERT_NAMESPACE);

        });

    }

  }

};

// Register and export component
Vue.component('hint-creation-dialog', comp);

// Expose the component and the vue instance used to create it (useful for testing)
export default { component: comp, vue: Vue };

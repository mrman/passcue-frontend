var should = require('should');
var TestUtil = require('../../../test/util');
var fixtures = require('../../../test/fixtures/alerts.js');
var JSPM = require('jspm');
var test = TestUtil.tape;

// Define the systemjs path to the component code
var COMPONENT_JS_PATH = 'app/components/hint-creation-dialog/component.js';

var Vue, Component;

test("hint-creation-dialog component", t => {

  // Import the component, using JSPM for uinit tests
  JSPM.import(COMPONENT_JS_PATH)
    .then(module => {
      t.ok(module.default.vue);
      t.ok(module.default.component);

      // Save Vue and component class to test-globals for later
      Vue = module.default.vue;
      Component = module.default.component;
    })
    .then(t.end)
    .catch(err => {
      console.log("Error occurred during setup:", err);
      t.fail(err);
    });
});

test("hint-creation-dialog component data is a function", t => {
  t.plan(1);
  t.assert( Component.data.should.have.type('function'), "Component data should be a function");
});

test("hint-creation-dialog component data spits out the correct fields and default values", t => {
  t.plan(5);

  var data = Component.data();
  t.assert( data.should.have.type('object'),  "Data function should return an object");
  t.assert( should(data.alerts).be.empty(), "Data.alert should be empty initially (not undefined)");
  t.assert( should(data.savingHintMessageShown).be.false(), "data.savingHintMessageShown should be false");
  t.assert( should(data.url).be.empty(), "data.url should be empty by default");
  t.assert( should(data.text).be.empty(), "data.text should be empty by default");
});

test("hint-creation-dialog component has the default data values after instantiation (with no params)", t => {
  t.plan(4);

  var vm = new Vue(Component).$mount();
  t.assert( should(vm.alerts).be.empty(), "Data.alert should be empty initially (not undefined)");
  t.assert( should(vm.savingHintMessageShown).be.false(), "data.savingHintMessageShown should be false");
  t.assert( should(vm.url).be.empty(), "data.url should be empty by default");
  t.assert( should(vm.text).be.empty(), "data.text should be empty by default");
});

test("hint-creation-dialog component properly server-side renders a single alert", t => {

  TestUtil.renderComponentWithData(Vue, Component, fixtures.ERROR)
    .then(w => {

      // Get the element and context
      var e = w.document.getElementsByClassName("hint-creation-dialog-component")[0];
      var content = e.textContent.trim();

      // Ensure expected fields are present
      var hintField = w.document.getElementById("url");
      var urlField = w.document.getElementById("text");
      t.assert( hintField.should.be.ok(), "Hint field should be present");
      t.assert( urlField.should.be.ok(), "Url field should be present");

      // Ensure create button is present
      var createBtn = w.document.getElementById("btn-create-hint");
      t.assert( createBtn.should.be.ok(), "Create hint button should be present");


    })
    .then(t.end)
    .catch(TestUtil.failAndEnd(t));
});

var should = require('should');
var TestUtil = require('../../../test/util');
var test = TestUtil.tape;

var COMPONENT_JS_PATH = 'app/components/hint-creation-dialog/component.js';

test("hint-creation-dialog component should $emit a created-hint event on successful hint creation", {timeout: 10000}, t => {

  TestUtil.renderModuleInJSDOM(COMPONENT_JS_PATH, "Component")
    .then(TestUtil.instantiateComponentFromLoadedGlobal("Component"))
    .then(TestUtil.addComponentElementToBody)
    .then(elemInfo => {
      // Set up fake hint info
      var url = "example.com";
      var text = "This is a test hint";

      // Get the elements for hint fields
      var urlField = elemInfo.window.document.getElementById("url");
      var textField = elemInfo.window.document.getElementById("text");
      var createBtn = elemInfo.window.document.getElementById("btn-create-hint");

      t.ok(urlField, "url field should be present on off-page element");
      t.ok(textField, "text field should be present on off-page element");
      t.ok(createBtn, "createBtn should be present on off-page element");

      // Set the values on the page
      elemInfo.vm.url = url;
      elemInfo.vm.text = text;

      // Ensure the values were taken up by VM
      t.equal(elemInfo.vm.url, url, "vm.url should have updated to fake hint url");
      t.equal(elemInfo.vm.text, text, "vm.text should have updated to fake hint text");

      // Watch for hint-created event to be emitted
      elemInfo.vm.$on('hint-created', hint => {
        t.pass("hint-created event emitted");

        // Ensure that the $emit-ed destroy contains the alert and alert ID we expect
        t.assert( hint.text.should.be.eql(text), "hint text should match entered value");
        t.assert( hint.url.should.be.eql(url), "hint url should match entered value");

        t.end();
      });

      // Click on the hint creation button, should create hint
      TestUtil.jsdomClick(createBtn, elemInfo.window);
    })
    .catch(TestUtil.failAndEnd(t));

});

import _ from "lodash";
import Vue from "vue";
import AppService from "app/services/app";
import style from "./style.css!";
import t from "./template.html!vtc";

const log = function() { console.log.apply(console, ["[footer-nav]", ...arguments]); };

const comp = {
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  data: function() {
    return {
      appName: AppService.getAppName() || null,
      currentYear: new Date().getFullYear()
    };
  }
};

// Register and export component
Vue.component('footer-nav', comp);

// Expose the component and the vue instance used to create it (useful for testing)
export default { component: comp, vue: Vue };


var should = require('should');
var TestUtil = require('../../../test/util');
var fixtures = require('../../../test/fixtures/alerts.js');
var JSPM = require('jspm');
var test = TestUtil.tape;

// Define the systemjs path to the component code
var COMPONENT_JS_PATH = 'app/components/footer-nav/component.js';

var Vue, Component;

test("footer-nav component", t => {

  // Import the component, using JSPM for uinit tests
  JSPM.import(COMPONENT_JS_PATH)
    .then(module => {
      t.ok(module.default.vue);
      t.ok(module.default.component);

      // Save Vue and component class to test-globals for later
      Vue = module.default.vue;
      Component = module.default.component;
    })
    .then(t.end)
    .catch(err => {
      console.log("Error occurred during setup:", err);
      t.fail(err);
    });
});

test("footer-nav component data is a function", t => {
  t.plan(1);
  t.assert( Component.data.should.have.type('function'), "Component data should be a function");
});

test("footer-nav component data spits out the correct fields and default values", t => {
  t.plan(2);

  var data = Component.data();
  t.assert( data.should.have.type('object'),  "Data function should return an object");
  t.assert( should(data.appName).not.be.empty(), "Data.appName should be non-empty");
});

test("footer-nav component properly server-side renders", t => {

  TestUtil.renderComponentWithData(Vue, Component, fixtures.ERROR)
    .then(w => {

      // Get the element and context
      var e = w.document.getElementsByClassName("footer-nav-component")[0];
      var content = e.textContent.trim();

      var year = new Date().getFullYear();

      // Check content
      t.assert( content.should.not.be.empty(), "Content should be non-empty");
      t.assert( content.should.containEql('© '), "should contain copyright symbol");
      t.assert( content.should.containEql(year), "should contain current year");
      t.assert( content.should.containEql('was built by '), "should contain note on builder");

    })
    .then(t.end)
    .catch(TestUtil.failAndEnd(t));
});

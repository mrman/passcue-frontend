import Vue from "vue";
import t from "./template.html!vtc";
import AppService from "app/services/app";
import UserService from "app/services/user";
import AlertService from "app/services/alert";
import EnvironmentService from "app/services/environment";
import PlainHeaderComponent from "app/components/plain-header/component";
import style from "./style.css!";

const log = function() { console.log.apply(console, ["[settings-page]", ...arguments]); };

const comp = {
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  beforeCreate: function() {
    // Redirect to login immediately if UserService does not have a valid API key
    if (!UserService.hasValidAPIKey()) {
      AppService.doNavigation(r => r.push('login'));
    }
  },

  data: function() {
    return {
      appName: AppService.appName,
      currentUserAPIKey: UserService.getAPIKey() || null,
      serverBaseURL: EnvironmentService.state.serverBaseURL
    };
  },

  computed: {
    serverBaseURLMatchesEnvSvc: function() {
      return EnvironmentService.state.serverBaseURL === this.serverBaseURL;
    },

    serverBaseURLBtnClass: function() {
      return this.serverBaseURLMatchesEnvSvc ? "button-success" : "button-error";
    },

    serverBaseURLBtnIconClass: function() {
      return this.serverBaseURLMatchesEnvSvc ? "fa-check" : "fa-save";
    }

  },

  methods: {

    // Save server base URL specifically
    saveServerBaseURL: function() {
      EnvironmentService.setServerBaseURL(this.serverBaseURL);
    },

    // Save all options as currently entered
    saveSettings: function() {
      this.saveServerBaseURL();
    }

  }

};

// Register and export component
Vue.component('settings-page', comp);
export default comp;

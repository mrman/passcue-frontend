import Vue from "vue";
import _ from "lodash";

const log = function() { console.log.apply(console, ["[app-info-service]", ...arguments]); };

var AppInfoService = new Vue({
  data: {
    state: {
      appName: "Passcue",
      router: null
    }
  },

  methods: {
    /**
     * Set the current app's router
     *
     * @param {Object} router - The (global) router to save
     */
    setRouter: function(router) {
      // Ensure email/password are provided
      if (_.isUndefined(router) || _.isNull(router)) {
        throw new Error("Invalid router object");
      }

      this.state.router = router;
    },

    /**
     * Utility function to nicely perform an action on the router (or fail if it doesn't exist)
     *
     * @param {Function} f - The function to perform on the router
     * @returns The result of executing the function on the router if there is one, null otherwise
     */
    doNavigation: function(f) {
      if (this.state.router) {
        return f.call(this, this.state.router);
      }
      return null;
    },

    getAppName: function() { return _.get(this, "state.appName", ""); }

  }
});

export default AppInfoService;

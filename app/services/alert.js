import Vue from "vue";
import _ from "lodash";

const log = function() { console.log.apply(console, ["[alert-service]", ...arguments]); };

var ALERT_ID = 0;

var AlertService = new Vue({
  data: {
    state: {
      alerts: {}
    }
  },

  methods: {

    /**
     * Add an alert
     *
     * @param {Object} alert - the alert to add
     * @param {string} [namespace] - the namespace to which to add the alert
     * @returns an identifier for the alert
     */
    addAlertForNamespace(alert, namespace) {
      var alertWithId = _.extend(alert, {id: ALERT_ID++});
      var alerts = this.getAlertsForNamespace(namespace);

      log(`Adding following alert to namespace ${namespace}:`, alert);

      // Add the alert
      alerts.push(alertWithId);
      return alertWithId.id;
    },

    /**
     * Clear all alerts for a given namespace
     *
     * @param {string} namespace - the namespace for which to clear all alerts
     */
    clearAlertsForNamespace: function(namespace) {
      var alertsForNamespace = _.get(this.state.alerts, namespace);
      alertsForNamespace.splice(0, alertsForNamespace.length);
    },

    // Clear all alerts for all namespaces
    clearAlerts: function() {
      _.each(this.state.alerts, (i, namespace) => {
        this.clearAlertsForNamespace(namespace);
      });
    },

    /**
     * Get all alerts for a given namespace
     *
     * @param {string} namespace - the namespace for which to get alerts
     */
    getAlertsForNamespace: function(namespace) {
      var key = `state.alerts.${namespace}`;
      // Create the alerts list if it doesn't exist yet
      if (_.isUndefined(_.get(this.state.alerts, namespace))) {
        _.set(this, key, []);
      }

      return _.get(this, key);
    },

    /**
     * Returns a (bound) function that splices the right array for the given namespace
     * This is to be used by components that want to affect this service's state
     *
     * @param {string} namespace - namespace
     * @returns a function that matches the signature of Array.prototype.splice
     */
    spliceAlertArrayForNamespace: function(namespace) {
      var localAlertArr = this.getAlertsForNamespace(namespace);
      return () => localAlertArr.splice.apply(localAlertArr, [...arguments]);
    },

    /**
     * Remove an alert in the given namespace
     *
     * @param {number} id - The ID of the alert to remove (given by AlertService)
     * @param {string} namespace - The namespace to which the ID belongs
     * @returns the number of remaining alerts after the removal
     */
    removeAlertForNamespace: function(id, namespace) {
      if (_.isUndefined(id) || _.isUndefined(namespace) || _.includes(namespace, ".")) {
        throw new Error("Missing or invalid ID/Namespace. Ensure the namespace does not contain dots.");
      }

      log(`Destroying alert with ID ${id} in namespace ${namespace}`);

      // Get the current list of alerts
      var alerts = _.get(this.state.alerts, namespace);
      if (_.isEmpty(alerts)) { return 0; }

      // Filter the list to remove the one with the id
      _.set(this.state.alerts, namespace, _.without(alerts, a => a.id == id));
      return alerts.length;
    }

  }

});

export default AlertService;

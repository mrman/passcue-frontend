import Vue from "vue";
import _ from "lodash";
import Util from "app/util";
import Constants from "app/constants";
import StateService from "app/services/state";
import EnvironmentService from "app/services/environment";

const LS_USER_SERVICE_STATE = "state-user-service";
const INIT_STATE = {apiKeyInfo: null};

const log = function() { console.log.apply(console, ["[user-service]", ...arguments]); };

var UserService = new Vue({
  data: function() {
    var state;

    // Attempt to parse api key info from storage
    try {
      state = StateService.getParsedJSONKVState(LS_USER_SERVICE_STATE);
    } catch (e) {}

    return { state: state || INIT_STATE };
  },

  methods: {

    /**
     * Save local state -- possibly to local storage or other client-side persistence.
     */
    saveState: function() {
      StateService.setKVState(LS_USER_SERVICE_STATE, JSON.stringify(this.state));
    },

    /**
     * Return whether the user service contains a valid API key
     *
     * @returns a boolean that represents whether the API key on the user service is currently valid
     */
    hasValidAPIKey: function() {
      var apiKeyInfo = _.get(this, "state.apiKeyInfo");
      if (Util.isUndefinedOrNull(apiKeyInfo) || _.isEmpty(apiKeyInfo)) { return false; }

      var {key, expires} = apiKeyInfo;
      var apiKeyNotUndefinedOrNull = !_.isUndefined(key) && !_.isNull(key);
      var apiKeyNotExpired = expires > _.now();

      return apiKeyNotUndefinedOrNull && apiKeyNotExpired;
    },

    /**
     * Set the current user's API key
     *
     * @param {Object} apiKeyInfo - An object containing information about the granted API key
     * @param {string} apiKeyInfo.key - The API key
     * @param {string} apiKeyInfo.secret - The API key secret
     * @param {string} apiKeyInfo.expires - The date (milliseconds since epoch) when the API key expires
     * @returns A promise that resolves to the value of the API key
     */
    setAPIKeyInfo: function(apiKeyInfo) {

      return new Promise((resolve, reject) => {
        // Ensure required keys are present, and key is not undefined or empty
        if (_.isUndefined(apiKeyInfo.key) || _.isUndefined(apiKeyInfo.expires) || _.isUndefined (apiKeyInfo.secret) ||
            !_.isString(apiKeyInfo.key) || _.isEmpty(apiKeyInfo.key.trim()) ||
            !_.isString(apiKeyInfo.secret) || _.isEmpty(apiKeyInfo.secret.trim()) ) {
          throw new Error("Invalid API key info:", apiKeyInfo);
        }

        // Set and save to localstorage
        this.state.apiKeyInfo = apiKeyInfo;

        this.saveState();
        resolve(this.state.apiKeyInfo);
      });
    },

    getAPIKey: function() { 
      return _.get(this, 'state.apiKeyInfo.key');
    },

    getAPIKeyInfo: function() { 
      return _.get(this, 'state.apiKeyInfo');
    },

    resetApiKeyInfo: function() {
      _.set(this, 'state.apiKeyInfo', {});
    },

    getUserId: function() { 
      return _.get(this, 'state.apiKeyInfo.userId');
    },

    getUserEmail: function() { 
      return _.get(this, 'state.apiKeyInfo.userEmail');
    },

    /**
     * Attempt to login with a given email and password
     *
     * @param {string} email
     * @param {string} password
     * @returns a Promise
     */
    login: function(email, password) {
      // Ensure email/password are provided
      if (_.isUndefined(email) || !_.isString(email) || _.isEmpty(email.trim())) {
        throw new Error("Invalid user email");
      }

      if (_.isUndefined(password) || !_.isString(password) || _.isEmpty(password.trim())) {
        throw new Error("Invalid user password");
      }

      // Trim email & pass
      email = email.trim();
      password = password.trim();

      // Get current base URL from env service
      var serverBaseURL = EnvironmentService.state.serverBaseURL;

      // POST login to API return result
      return new Promise(function(resolve, reject) {
        Util.postJSONToAPI(serverBaseURL, Constants.URLS.LOGIN, {email, password})
          .then(resolve)
          .catch(reject);
      });

    },

    /**
     * Attempt to signup with a given email and password
     *
     * @param {string} email
     * @param {string} password
     * @returns a Promise
     */
    signup: function(email, password) {
      // Ensure email/password are provided
      if (_.isUndefined(email) || !_.isString(email) || _.isEmpty(email.trim())) {
        throw new Error("Invalid user email");
      }

      if (_.isUndefined(password) || !_.isString(password) || _.isEmpty(password.trim())) {
        throw new Error("Invalid user password");
      }

      // Trim email & pass
      email = email.trim();
      password = password.trim();

      // Get current base URL from env service
      var serverBaseURL = EnvironmentService.state.serverBaseURL;

      // POST signup to API return result
      return new Promise(function(resolve, reject) {
        Util.postJSONToAPI(serverBaseURL, Constants.URLS.USERS, {email, password})
          .then(resolve)
          .catch(reject);
      });

    },

    // Log out the current user
    logout: function() {
      return new Promise((resolve, reject) => {
        // Do logout
        var currentApiKeyInfo = this.getAPIKey();
        this.resetApiKeyInfo();
        this.saveState();

        resolve(currentApiKeyInfo);
      });
    }

  }
});

export default UserService;

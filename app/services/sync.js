import Vue from "vue";
import _ from "lodash";
import Util from "app/util";
import Constants from "app/constants";
import StateService from "app/services/state";
import UserService from "app/services/user";
import EnvironmentService from "app/services/environment";

const LS_KEY_SERVICE_STATE = "state-sync-service";
const DEFAULT_SYNC_TIMEOUT_MILLISECONDS = 1000; // Attempt to sync every second

const SYNC_OPERATIONS = ["ADD_HINT", "REMOVE_HINT"];
const isValidSyncOperation = _.partial(_.includes, SYNC_OPERATIONS);

const OP_RETRY_MAX = 5;

const EXPECTED_SYNC_OBJECT_FIELDS = ["id", "op", "data"];
const isValidSyncObject = _.flow(
  _.keys,
  // Map every element to whether it's included in EXPECTED_SYNC_OBJECT_FIELDS
  _.partialRight(_.map, _.partial(_.includes, EXPECTED_SYNC_OBJECT_FIELDS)),
  _.every
);

const SYNC_LOG_EMPTY_ERROR_MSG = "Sync Log is empty";

const log = function() { console.log.apply(console, ["[sync-service]", ...arguments]); };

/**
 * Syncs the changes made in the local application to the backend server.
 * The sync service primarily contains a log of operations that it tries to sync to server as it is running,
 * Attempting to sync checking every DEFAULT_SYNC_TIMEOUT_MILLISECONDS
 */
var SyncService = new Vue({
  data: function() {
    var state = StateService.getParsedJSONKVState(LS_KEY_SERVICE_STATE);
    var log = _.get(state, "log", []);

    return {
      operationHandlers: {
        "ADD_HINT": this.handleAddHint,
        "REMOVE_HINT": this.handleRemoveHint
      },
      syncIntervalFn: null,
      opFailureCounter: {},

      state: {
        backendHostname: "/",
        log: log,
        syncAttemptTimeoutMilliseconds: DEFAULT_SYNC_TIMEOUT_MILLISECONDS
      }
    };
  },

  created: function() {
    var timeout = this.state.syncAttemptTimeoutMilliseconds;

    // Attempt to sync the first operation in the sync log every syncAttemptTimeoutMilliseconds
    if (Util.inBrowserContext()) {
      this.syncIntervalFn = setInterval(this.syncFirstOperation, timeout);
    }
  },

  methods: {
    // Save local state -- possibly to local storage or other client-side persistence.
    saveState: function() { StateService.setKVState(LS_KEY_SERVICE_STATE, JSON.stringify(this.state)); },

    /**
     * Add an operation that should be synced to the server
     *
     * @param {SyncObject} o - The operation representing what was done to the hint store locally
     * @param {Object} o.op - Type of operation, should be a value in SYNC_OPERATIONS
     * @param {Object} o.data - Data required by the operation
     * @returns A promise that resolves to the added operation (as returned by the server)
     */
    addOperationToLog: function(o) {
      return new Promise((resolve, reject) => {
        // Give operation an ID if not provided
        if (Util.isUndefinedOrNull(_.get(o, "id"))) {
          o.id = Util.generateUUID();
        }

        // Ensure sync object is valid
        if (!isValidSyncObject(o)) {
          reject(new Error("Invalid Sync Object:", o));
          return;
        }

        // Ensure the operation is in the list of allowed operations
        if (!isValidSyncOperation(o.op)) {
          reject(new Error("Invalid Sync operation [" + o.op + "]"));
          return;
        }

        // Add the operation to the log, save state
        this.state.log.push(o);
        this.saveState();
        resolve(o);
      });

    },

    /**
     * Attempt to sync changes with the backend service.
     * As changes sync, state.log should lose entries
     *
     * @returns a promise that resolves to the sync'd operation (as returned by the server)
     */
    syncFirstOperation: function() {
      // If the sync log is empty, return an immediately resolved promise with an empty object
      if (_.isEmpty(this.state.log)) {
        return Promise.resolve({});
      }

      // Take the first operation out of the sync log
      var o = this.state.log[0];

      return new Promise((resolve, reject) => {
        // Ensure sync object is valid
        if (!isValidSyncObject(o)) {
          reject(new Error("Invalid Sync Object:", o));
          return;
        }

        // Ensure the operation is in the list of allowed operations
        if (!isValidSyncOperation(o.op)) {
          reject(new Error("Invalid Sync operation [" + o.op + "]"));
          return;
        }

        // Find the appropriate handler for the operation
        var handler = this.operationHandlers[o.op];
        if (Util.isUndefinedOrNull(handler)) {
          reject(new Error("Failed to find sync operation handler for operation [" + o.op + "]"));
          return;
        }

        // Call the handler with the given data
        handler(o)
          .then(d => {
            // Remove the op if it succeeded
            this.removeOpByID(o.id);

            return d; // pass on the data
          })
          .then(resolve)
          .then(() => this.saveState())
          .catch((err) => {

            // If an op fails OP_RETRY_MAX times, remove it.
            this.opFailureCounter[o.id] = this.opFailureCounter[o.id] || 0;

            if (++this.opFailureCounter[o.id] >= OP_RETRY_MAX) {
              // Remove the op that's failed
              this.removeOpByID(o.id);
            }

            reject(err);
          });
      });


    },

    removeOpByID: function(id) {
      // If handling the operation succeeded, remove the operation from the log
      var opIdx = _.findIndex(this.state.log, i => i.id === id);
      if (opIdx >= 0) {
        return this.state.log.splice(opIdx, 1);
      }

      return null;
    },

    /**
     * Handle the addition of a hint.
     *
     * @param {SyncOperation} op - The original sync operation
     * @param {Hint} op.data - The hint object
     * @returns a promise which resolves to the added hint
     */
    handleAddHint: function(op) {
      var hint = _.get(op, "data");

      // TODO: optimization, don't attempt to add a hint that is going to be removed but hasn't been synced yet.
      if (Util.isUndefinedOrNull(hint) || !Util.isValidHint(hint)) {
        return Promise.reject(new Error("Failed to sync invalid hint to server"));
      }

      // Get the user's IDor fail immediately
      var userId = UserService.getUserId();
      var authHeaders = Util.genAuthHeadersFromAPIKeyInfo(UserService.getAPIKeyInfo());
      if (Util.isUndefinedOrNull(userId) || Util.isUndefinedOrNull(authHeaders)) {
        return Promise.reject(new Error("Auth failure for current user: userID or auth headers invalid"));
      }

      // Get current base URL from env service
      var serverBaseURL = EnvironmentService.state.serverBaseURL;

      return new Promise((resolve, reject) => {

        // Attempt to add hint to API
        return Util.postJSONToAPI(serverBaseURL,
                                  Util.getHintsUrlForUserWithId(userId),
                                  hint,
                                  authHeaders)
          .then(Util.jsonifyFetchResponse)
          .then(resp => {

            if (_.get(resp, "status") != "success") {

              reject(new Error("Attempt to sync with database failed"));

            } else {

              // Ensure that the hint was removed from the hint service
              this.$emit('synced-add', {hint: hint});
              resolve(hint);

            }
          })
          .catch(reject);

      });
    },

    /**
     * Handle the removal of a hint
     *
     * @param {SyncOperation} op - The original sync operation to perform
     * @param {string} op.data - ID of the hint to be removed
     * @returns a promise which resolves to the removed hint
     */
    handleRemoveHint: function(op) {
      var hintId = _.get(op, "data");

      if (_.isObject(hintId)) {
        hintId = hintId.id;
      }

      if (Util.isUndefinedOrNull(hintId)) {
        return Promise.reject(new Error("Failed to sync invalid hint ID"));
      }

      // Get the user's ID & generate auth headers or fail immediately
      var userId = UserService.getUserId();
      var authHeaders = Util.genAuthHeadersFromAPIKeyInfo(UserService.getAPIKeyInfo());
      if (Util.isUndefinedOrNull(userId) || Util.isUndefinedOrNull(authHeaders)) {
        return Promise.reject(new Error("Auth failure for current user: userID or auth headers invalid"));
      }

      // Get current base URL from env service
      var serverBaseURL = EnvironmentService.state.serverBaseURL;

      return new Promise((resolve, reject) => {

        // Attempt to delete hint from API
        return Util.deleteFromAPI(serverBaseURL,
                                  Util.getSingleHintUrlForUserWithId(userId, hintId),
                                  authHeaders)
          .then(Util.jsonifyFetchResponse)
          .then(resp => {

            // Fail if returned status was unsuccessful
            if (_.get(resp, "status") != "success") {
              reject(new Error("Attempt to sync with database failed"));
              return;
            }

            // Ensure that the hint was removed from the hint service
            this.$emit('synced-delete', {hintId: hintId});
            resolve(hintId);
          })
          .catch(reject);
      });

    },

    getLog: function() {
      return this.state.log;
    },

    clearLog: function() {
      _.set(this, 'state.log', []);
      this.saveState();
    }

  }
});

export default SyncService;

import Vue from "vue";
import _ from "lodash";
import Constants from "app/constants";
import StateService from "app/services/state";

const LOCAL_STORAGE_KEY_ENVIRONMENT = "state-environment-service";

const log = function() { console.log.apply(console, ["[environment-service]", ...arguments]); };

const DEFAULT_ENV_NAME = Constants.ENV_NAMES.NODE;
const ENV_NAME_TESTS = [
  [Constants.ENV_NAMES.CHROME_EXTENSION, () => window.location.toString().includes("chrome-extension")],
  [Constants.ENV_NAMES.FIREFOX_EXTENSION, () => window.location.toString().includes("moz-extension")]
];

// Default server address should be nothing (current server)
const DEFAULT_SERVER_BASE_URL = "";
const DEFAULT_STATE = {serverBaseURL: DEFAULT_SERVER_BASE_URL};

/**
 * This service contains information about the current environment
 *
 * @class EnvironmentService
 */
var EnvironmentService = new Vue({
  data: function() {
    var envName = this.findEnvName();
    var state = StateService.getParsedJSONKVState(LOCAL_STORAGE_KEY_ENVIRONMENT);

    return {
      envName: envName,
      state: state || DEFAULT_STATE
    };
  },

  methods: {

    saveState: function() {
      StateService.setKVState(LOCAL_STORAGE_KEY_ENVIRONMENT, JSON.stringify(this.state));
    },

    setServerBaseURL: function(url) {
      _.set(this.state, "serverBaseURL", url);
      this.saveState();
    },

    /**
     * Determine, if possible, the kind of environment the app is being run in
     *
     * @returns the env name, which should be an element of Constant.ENV_NAMES
     */
    findEnvName: function() {
      try {
        // Find first passing tuple witwhich tells us the appropriate environment name
        var passingTuple =  _.find(ENV_NAME_TESTS, t => t[1]());

        return passingTuple ? passingTuple[0] : DEFAULT_ENV_NAME;
      } catch(e) {
        return DEFAULT_ENV_NAME;
      }
    }

  }
});

export default EnvironmentService;

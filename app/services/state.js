import Vue from "vue";
import _ from "lodash";

const log = function() { console.log.apply(console, ["[state-service]", ...arguments]); };

/**
 * This service proxies all state-saving.
 * Will use a mock in-memory object if other storage methods aren't available (ex. testing with jsdom)
 *
 * Note that underlying storage mechanisms (ex. `localStorage`) must be wrapped in try/catch as they will
 * cause errors when this module is bundled by SystemJS in a NON-browser-like context. In those contexts,
 * `localStorage` will not exist and SystemJS will error
 *
 * @class StateService
 */
var StateService = new Vue({
  data: function() {
    return {
      mockStorage: {}
    };
  },

  methods: {

    /**
     * Save key/value based state (in localstorage or mock)
     *
     * @param {string} k - Key
     * @param {Object} v - Value to be saved for the given key
     * @returns An tuple of the passed in/saved key and value
     */
    setKVState: function(k,v) {
      try {
        localStorage.setItem(k, v);
      } catch (e) {
        this.mockStorage[k] = v;
      }

      return [k,v];
    },

    /**
     * Get a value from state (localstorage or mock) based on key
     *
     * @param {string} k
     * @returns The value of the associated key, or null
     */
    getKVState: function(k) {
      try {
        return localStorage.getItem(k);
      } catch (e) {
        return this.mockStorage[k] || null;
      }
    },

    getParsedJSONKVState: function(k) {
      return JSON.parse(this.getKVState(...arguments));
    }

  }
});

export default StateService;

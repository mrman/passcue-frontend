import Vue from "vue";
import _ from "lodash";
import URI from "jsuri";
import Util from "app/util";
import UserService from "app/services/user";
import Constants from "app/constants";
import StateService from "app/services/state";
import SyncService from "app/services/sync";
import EnvironmentService from "app/services/environment";

const LOCAL_STORAGE_KEY_HINTS = "state-hint-service-hints";

// ID used for locally created hints that haven't bene synced yet
const LOCAL_HINT_ID = 1;

const log = function() { console.log.apply(console, ["[hint-service]", ...arguments]); };

var HintService = new Vue({
  data: function() {
    var hints = StateService.getParsedJSONKVState(LOCAL_STORAGE_KEY_HINTS);

    // If the Sync serice ever deletes a hint, make sure it's deleted locally as well
    SyncService.$on('synced-delete', info => {
      var hintId = info.hintId;
      if (Util.isUndefinedOrNull(hintId)) {
        return;
      }

      this.removeHintById(hintId, {skipSync: true});

    });

    return {
      state: {
        // Hints is a mapping of domains => hints for efficiency/partitioning
        hints: hints || [],
        apiKeyInfo: UserService.getAPIKeyInfo()
      }
    };
  },

  computed: {

    // Fast hint lookup
    hintIdLookup: function() {
      var regularIds =  _.fromPairs(_.map(this.state.hints, h => [h.id, h]));
      var clientIds = _.fromPairs(_.map(this.state.hints, h => [h.clientId, h]));
      return _.merge(regularIds, clientIds);
    }

  },

  methods: {

    /**
     * Save local state -- possibly to local storage or other client-side persistence.
     */
    saveState: function() {
      StateService.setKVState(LOCAL_STORAGE_KEY_HINTS, JSON.stringify(this.state.hints));
    },

    /**
     * Fetch hints for the current user from the backend
     *
     */
    fetchHints: function() {
      var comp = this;
      var apiKeyInfo = _.get(this, "state.apiKeyInfo");

      // Ensure api key info is valid
      if (_.isUndefined(apiKeyInfo) || _.isNull(apiKeyInfo)) {
        throw new Error("Invalid api key info:", this.apiKeyInfo);
      }

      // Get current base URL from env service
      var serverBaseURL = EnvironmentService.state.serverBaseURL;

      // Get hints
      return Util.getJSONFromAPI(serverBaseURL,
                                 `users/${apiKeyInfo.userId}/hints`,
                                 Util.genAuthHeadersFromAPIKeyInfo(apiKeyInfo))
        .then(Util.jsonifyFetchResponse)
        .then(resp => {

          if (resp.status === "success" && _.has(resp, "data") && !_.isEmpty(resp.data)) {
            log("Adding hint data to local repository:", resp.data);

            _(resp.data)
              .map(comp.processHint)
              .each(comp.addHint);
          }

        });
    },

    /**
     * Create a hint
     *
     * @param {string} url - Create hint
     * @param {string} hintText - The hint to save
     * @returns A hint object, without expiration date
     */
    createHint: function(url, hintText) {
      return {
        clientId: Util.generateUUID(),
        url: url,
        text: hintText,
        created: new Date()
      };
    },

    // Get all hints
    getHints: function() {
      // Hints that is passed back isn't reactive possibly?
      return this.state.hints;
    },

    /**
     * Gets hints for a domain
     *
     * @param {string} domain - The domain to use, expected to be of form <something>.tld
     * @params a list of hints for that domain
     */
    getHintsForDomain: function(d) {
      return _.filter(this.state.hints, h => h.url === d);
    },

    /**
     * Check if this service contains the given hint
     *
     * @param {object} hint - The hint to check for
     * @returns Whether this service has an identical hint
     */
    containsHint: function(hint) {
      var exists =  _.some(this.state.hints, _.partial(Util.hintsAreSame, hint));
      return exists;
    },

    /**
     * Add a hint
     *
     * @param {Hint} h - The hint to attempt to add
     * @returns A promise that resolves when the hint has been added, to the hint + any backend changes
     */
    addHint: function(h) {
      return new Promise((resolve, reject) => {
        // Resolve domain and replace the URL with the domain
        var domain = new URI(h.url).host();
        h.url = domain;

        // Ensure the hint has an ID
        if (Util.isUndefinedOrNull(h.id) && Util.isUndefinedOrNull(h.clientId)) {
          throw new Error("Invalid Hint, expected to have at least one ID (server/client specified)");
        }

        // Add the hint, if id is present and no copy already exists
        if (!this.containsHint(h)) {

          this.state.hints.push(h);

          // Attempt to sync this op to server eventually
          // All saves shoudl go to the Sync service log at least before they're even saved locally
          SyncService.addOperationToLog({op: "ADD_HINT", data: h})
            .then(o => {
              // Save hint locally
              this.saveState();

              // Resolve the addHint promise with the original hint if adding sync op did not reject
              resolve(h);
            })
            .catch(reject);

        } else {

          // If the hint already exists, update the existing one
          // TODO: Move this out to an create-or-update type function
          var existingHint = this.state.hints.find(existingHint => existingHint.id === h.id || existingHint.clientId === h.id);
          existingHint = _.extend(existingHint, h);

          resolve(existingHint);
        }


      });
    },

    // Clear remove all hints
    clearHints: function() {
      _.set(this, 'state.hints', []);
      this.saveState();
    },

    /**
     * Remove a hint object
     *
     * @param {Hint} h - The hint object to remove
     * @param {Object} options - Execution options
     * @param {boolean} options.skipSync - Skip attempting to sync
     * @returns The hint object that was removed, otherwise null
     */
    removeHint: function(h, opts) {
      return this.removeHintById(h.id, opts) ? h : null;
    },

    /**
     * Remove a hint object by ID
     *
     * @param {String} id - The ID of the hint object to remove
     * @param {Object} options - Execution options
     * @param {boolean} options.skipSync - Skip attempting to sync
     * @returns The hint object that was removed, otherwise null
     */
    removeHintById: function(id, opts) {
      var idx = _.findIndex(this.state.hints, h => h.id === id);
      var skipSync = _.get(opts, "skipSync", false);

      if (idx < 0) {
        // At this point, hint may already be deleted if it was deleted locally but not on server
        // on startup, SyncService would attempt to delete again and trigger $on
        return Promise.reject(new Error("Failed to find target of hintRemoval " + idx));
      }

      return new Promise((resolve, reject) => {
        // Attempt to remove the hint
        this.state.hints.splice(idx, 1);

        // Attempt to replicate changes if the hint was saved server side
        if (!skipSync) {
          SyncService.addOperationToLog({op: "REMOVE_HINT", data: id})
            .then(o => {
              this.saveState();
              resolve(id);
            });
        } else {
          this.saveState();
          resolve(id);
        }

      });
    }

  }
});

export default HintService;

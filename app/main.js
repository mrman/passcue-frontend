import _ from "lodash";
import Vue from "vue";
import VueRouter from "vue-router";
import t from "app/main.html!vtc";
import EnvironmentService from "app/services/environment";
import LoginPage from "app/components/login-page/component";
import SignupPage from "app/components/signup-page/component";
import LandingPage from "app/components/landing-page/component";
import AppPage from "app/components/app-page/component";
import SettingsPage from "app/components/settings-page/component";
import AppService from "app/services/app";
import Util from "app/util";

import 'purecss/build/pure-min.css!';
import 'purecss/build/grids-responsive-min.css!';
import 'static/fonts/font-awesome-4.7.0/css/font-awesome.min.css!';
import style from "./main.css!";

// Build router
Vue.use(VueRouter);
const routes = [
  { name: 'landing', path: "/", component: LandingPage },
  { name: 'login', path: "/login", component: LoginPage },
  { name: 'app', path: "/app", component: AppPage },
  { name: 'settings', path: "/settings", component: SettingsPage },

  { name: 'signup', path: "/signup", component: SignupPage,
    props: r => ({initialEmail: r.query.emailAddress}) },

  // Catchall for any route that doesn't match, redirects to landing page
  { path: "*", redirect: {name: 'landing'} }
];

const router = new VueRouter({ routes });
AppService.setRouter(router);

// Build & mount app
var vueApp = new Vue({
  render: t.render,
  staticRenderFns: t.staticRenderFns,

  data: {
    appEnvName: EnvironmentService.envName
  },

  computed: {
    appEnvironmentNameClass: function() {
      return "app-env-" + this.appEnvName.toLocaleLowerCase();
    }
  },

  router: router
 }).$mount("#app-container");

// Set the Vue app on the global namespace.
if (!_.isUndefined(window)) {
  window.Vue = Vue;
  window.vueApp = vueApp;
}
